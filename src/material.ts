import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material';
import {MatListModule} from '@angular/material/list';
import {MatRadioModule} from '@angular/material/radio';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatTableModule} from '@angular/material/table';
import {MatStepperModule} from '@angular/material/stepper';
import {MatSidenavModule} from '@angular/material/sidenav';
import { NgModule } from '@angular/core';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatNativeDateModule, MatSliderModule, DateAdapter} from '@angular/material';

@NgModule({
  imports: [
    MatSnackBarModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatSliderModule,
    MatNativeDateModule,
    MatSidenavModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatInputModule,
    MatFormFieldModule,
    MatBadgeModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatMenuModule,
    MatCardModule,
    MatExpansionModule,
    MatDividerModule,
    MatListModule,
    MatRadioModule,
    MatDatepickerModule,
    MatTooltipModule,
  ],
  exports: [
    MatSnackBarModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatSliderModule,
    MatNativeDateModule,
    MatSidenavModule,
    MatStepperModule,
    MatTableModule,
    MatInputModule,
    MatFormFieldModule,
    MatBadgeModule,
    MatIconModule,
    MatTabsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatMenuModule,
    MatListModule,
    MatRadioModule,
    MatCardModule,
    MatExpansionModule,
    MatDividerModule,
    MatDatepickerModule,
    MatTooltipModule,
  ]
})
export class MyOwnCustomMaterialModule {}

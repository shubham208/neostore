import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { DataService } from '../data.service';
import { Data2Service } from '../data2.service';
import * as _ from 'lodash';

@Component({
    selector: 'app-address',
    templateUrl: './address.component.html',
    styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {
    public add = [];
    show;
    // userInfo = JSON.parse(localStorage.getItem('userData'));
    constructor(private route: Router, private addressService: RegisterService, private data: DataService,

    ) { }

    ngOnInit() {
        /**
         * API call => get all the address
         */
        this.addressService.getAddress().toPromise().then((data) => {
            this.add = data.customer_address;
            console.log(data.customer_address);
            this.show = true;
            // this.route.navigate(['/adress']);
        }).catch((error) => {
            console.log('errrorrrr');
            this.show = false;
        });
    }
    editPage(id) {
        console.log(id);
        this.data.setLocalStorage('tempAdd', id);
        this.route.navigate(['/editAddress/' + id.address_id]);

    }
    /**
     * navigates to the add address component
     */
    deleteAddress(address, i) {
        const userInfo = this.data.getLocalStorage('userData');
        console.log((userInfo.customer_address).length);
        console.log(userInfo);

        if (confirm('Are you sure you want to delete this address')) {
            this.addressService.delAddress(address).toPromise().then((data: any) => {
                document.getElementById(i).style.display = 'none';
                console.log((userInfo.customer_address).length);
                userInfo.customer_address = userInfo.customer_address.filter((value) => {
                    return value.address_id !== address.address_id;

                });
                this.data.setLocalStorage('userData', userInfo);
                if ((userInfo.customer_address).length === 0) {
                    // this.data1.displayAdd(false);
                    this.show = false;


                }


                // console.log(a);

                // userInfo.customer_address.forEach((element, j) => {
                //     if (address.id === element._id) {
                //         console.log('del');
                //         // console.log(userInfo.customer_address.findIndex(element._id));

                //         userInfo.customer_address.splice(j, 1);
                //         this.data.setLocalStorage('userData', userInfo);
                //         console.log('del', this.data.getLocalStorage('userData').customer_address);

                //         if ((userInfo.customer_address).length === 0) {
                //             // this.data1.displayAdd(false);
                //             this.show = false;


                //         }
                //         // if (!_.findIndex(userInfo.customer_address, { _id: data.address_id })) {
                //         //     console.log('1');
                //         //     userInfo.customer_address.splice(j, 1);


                //         // } else {
                //         //     console.log('2');


                //         // }

                //         // localStorage.setItem('userData', JSON.stringify(userInfo));
                //         // this.data1.count(this.userInfo.customer_address.length);
                //     }
                // });
            }).catch((error) => {
            });

        }
    }
    addAddress() {
        this.route.navigate(['/addAddress']);
    }


}

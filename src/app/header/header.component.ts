import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { MyOwnCustomMaterialModule } from 'src/material';
import { RegisterService } from '../register.service';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Data2Service } from '../data2.service';
import { MatSnackBar } from '@angular/material';
import { userInfo } from 'os';
import { debounceTime } from 'rxjs/operators';
import { fromEvent } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    search = new FormControl('');
    url: string;
    options = [];
    prod: any;
    show: string;
    msg: string;
    profileImg = this.data.getLocalStorage('customerDetails');
    userInfo = this.data.getLocalStorage('userData');
    filteredOptions: Observable<string[]>;
    count: any;
    pic;
    // myCart = JSON.parse(localStorage.getItem('cart'));
    myCartCount = '';
    searchBox;
    keyup$;
    keycode;
    // userInfo = JSON.parse(localStorage.getItem('userData'));
    // cartCount = this.myCart.length;
    constructor(
        private data: DataService,
        public route: Router,
        private apiService: RegisterService,
        private data1: Data2Service,
        private snackBar: MatSnackBar,
        private spinner: NgxSpinnerService) { }

    ngOnInit() {
        this.apiService.authentication().toPromise().then((data) => {
            this.data.changeMessage('show');
        }).catch((error) => {
            this.data.changeMessage('hide');
            this.data.setLocalStorage('cart', []);
            localStorage.removeItem('userData');
            localStorage.removeItem('customerDetails');

        });
        this.cartCount();
        this.data.pic.subscribe((data) => {
            console.log('---->>>>', data);
            if (data === '') {
                console.log('grsdg', data);
                console.log(this.profileImg);
                if (this.profileImg != null) {
                    if (this.profileImg.profile_img == null) {
                        this.pic = '../../assets/images/profile-placeholder.png';
                    } else {
                        this.pic = this.apiService.baseUrl + this.profileImg.profile_img;
                        console.log(this.pic);
                    }
                }
            } else {
                this.pic = this.apiService.baseUrl + data;
            }
            if (data == null) {
                this.pic = '../../assets/images/profile-placeholder.png';
            }
        });
        this.url = this.apiService.baseUrl;
        this.apiService.getAllProducts().subscribe((data) => {
            data.product_details.map((item) => {
                this.options.push(item.product_name);
            });
            // console.log('options', this.options);
        });
        this.filteredOptions = this.search.valueChanges
            .pipe(
                startWith(''),
                map(value => this._filter(value))
            );

        this.data.currentMessage.subscribe(msg => {
            if (msg === '') {
                console.log('hide');
                this.show = 'hide';
                if (this.data.getLocalStorage('userData')) {
                    console.log('show');
                    this.show = 'show';
                }
            } else {
                console.log('header', msg);
                if (msg === 'show' || msg === 'hide') {
                    this.show = msg;
                }
            }
        });
        this.cartCount();
        /**
         * -----------------Debaunce Concept-------------------
         */
        // elem ref
        // this.searchBox = document.getElementById('search');
        // // streams
        // this.keyup$ = fromEvent(this.searchBox, 'keyup');
        // // wait .5s between keyups to emit current value
        // this.keyup$
        // .pipe(
        //   map((i: any) => i.currentTarget.value),
        //   debounceTime(100)
        // )
        // .subscribe((data) => {
        //   console.log('key up ', data);
        //   console.log(this.keyup$);
        //   this.apiService.getAllProducts('', '', '', '', data).subscribe((item) => {
        //         // console.log('search', data);
        //         if ( this.keycode === 13 ) {
        //           console.log('enter button');
        //           // console.log(this.search.value);
        //           this.prod = item;
        //           this.route.navigate(['/productPage', {item1: JSON.stringify(this.prod)}]);
        //           this.search.setValue('');
        //         }
        //       });
        // });

    }
    /**
     * To get the key code
     * @param $event => key up event
     */
    searchKey($event) {
        console.log($event);
        this.keycode = $event.keyCode; // return the key code of the key pressed
    }
    /**
     * To get the cart count of the cart
     */
    cartCount(): void {
        this.data1.cartCount.subscribe((data) => {
            console.log('data2 service', data);
            if (data) {
                this.myCartCount = data;
            } else {
                if (this.data.getLocalStorage('cart')) {
                    console.log(data, '555');
                    this.myCartCount = this.data.getLocalStorage('cart').length;
                    // console.log(this.myCartCount, 'hellloooooooooo');
                } else {
                    this.myCartCount = '0';
                }
            }
        });

    }
    cart() {
        this.route.navigate(['/mainCart']);
    }
    /**
     * Search Box Autocomplete Filter to get the products according to the word type
     * @param value => input value
     */
    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase(); // converts the text to lower case
        return this.options.filter(option => option.toLowerCase().includes(filterValue));
    }
    order() {
        this.route.navigate(['/order']);

    }
    login() {
        console.log('as');
        this.route.navigate(['/login']);

    }
    register() {
        this.route.navigate(['/register']);

    }
    productPage() {
        this.route.navigate(['/productPage']);
    }
    home() {
        this.route.navigate(['/dashboard']);
    }
    profile() {
        this.route.navigate(['/profile']);
    }
    logout() {
        this.apiService.logout();
    }
    /**
     * SearchBar Function
     * @param e keyUp event
     */
    searchBar(e) {
        if (e.keyCode === 13) {
            console.log('enter button');
            this.apiService.getAllProducts('', '', '', '', this.search.value).toPromise().then((data: any) => {
                console.log(this.search.value);
                this.prod = data;
                console.log('searchdata', data);
                this.apiService.serachProd = data;
                this.route.navigate(['/productPage/' + data.product_details[0].product_name]);
                this.search.setValue('');
            })
                .catch((error) => {
                });
        }
    }
}

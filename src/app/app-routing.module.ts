import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductPageComponent } from './product-page/product-page.component';
import { CartComponent } from './cart/cart.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProfileComponent } from './profile/profile.component';
import { OrdersComponent } from './orders/orders.component';
import { AddressComponent } from './address/address.component';
import { AddAddressComponent } from './add-address/add-address.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { MainCartComponent } from './main-cart/main-cart.component';
import { LocationComponent } from './location/location.component';
import { SubscriberComponent } from './subscriber/subscriber.component';
import { ProfileHomeComponent } from './profile-home/profile-home.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AuthGuard } from './auth.guard';
import { OrderPlacedComponent } from './order-placed/order-placed.component';
import { RateProductComponent } from './rate-product/rate-product.component';
import { CanDeactivateGuard } from './canDeactivateGuard';
import { DemoComponent } from './demo/demo.component';
import { EditAddressComponent } from './edit-address/edit-address.component';
import { DocumentComponent } from './document/document.component';
import { ContactComponent } from './contact/contact.component';
import { ForgotPassComponent } from './forgot-pass/forgot-pass.component';
import { RecoverPassComponent } from './recover-pass/recover-pass.component';
import { ChangePassComponent } from './change-pass/change-pass.component';



const routes: Routes = [
    { path: 'dashboard', component: DashboardComponent },
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    // { path: 'productPage', redirectTo: 'productPage/:id'},

    { path: 'productPage', component: ProductPageComponent },
    { path: 'productPage/:id', component: ProductPageComponent },

    { path: 'productDetails/:id', component: ProductDetailsComponent },
    { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
    // { path: 'cart', component: CartComponent},
    { path: 'register', component: RegisterComponent, canDeactivate: [CanDeactivateGuard] },
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
    { path: 'order', component: OrdersComponent, canActivate: [AuthGuard] },
    { path: 'adress', redirectTo: 'address' },

    { path: 'address', component: AddressComponent },
    { path: 'contact', component: ContactComponent },

    { path: 'addAddress', component: AddAddressComponent, canActivate: [AuthGuard] },
    { path: 'editAddress/:id', component: EditAddressComponent },
    // { path: 'document/:id', component: DocumentComponent},

    { path: 'editProfile', component: EditProfileComponent },
    { path: 'mainCart', component: MainCartComponent },
    { path: 'location', component: LocationComponent },
    { path: 'subscriber', component: SubscriberComponent },
    { path: 'error-page/:id', component: ErrorPageComponent },
    { path: 'order-placed', component: OrderPlacedComponent },
    { path: 'recover-pass', component: RecoverPassComponent, canActivate: [AuthGuard] },

    { path: 'rate-product', component: RateProductComponent },
    { path: 'forgot-pass', component: ForgotPassComponent, canActivate: [AuthGuard] },
    { path: 'change-pass', component: ChangePassComponent, canActivate: [AuthGuard] },

    { path: 'demo', component: DemoComponent },
    { path: '**', component: ErrorPageComponent }




    // { path: 'profile', component: ProfileHomeComponent},

];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }

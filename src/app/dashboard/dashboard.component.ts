import { Component, OnInit } from '@angular/core';
import { ProductInfoService } from '../product-info.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

// declare var $:JQueryStatic;
import * as $ from 'jquery';
import { TopRatedService } from '../top-rated.service';
import { RegisterService } from '../register.service';
import { MatSnackBar } from '@angular/material';
import { DataService } from '../data.service';
import { Data2Service } from '../data2.service';
import { userInfo } from 'os';
import { HeaderComponent } from '../header/header.component';
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    abcd: any;
    topProd = [];
    categoryImg = [];
    prod = [];
    show: string;

    count: any;
    // c = JSON.parse(localStorage.getItem('cart'));
    // userInfo = JSON.parse(localStorage.getItem('userData'));

    // userData = JSON.parse(localStorage.getItem('userData'));
    constructor(
        private spinner: NgxSpinnerService,
        private data: DataService,
        private snackBar: MatSnackBar,
        private products: RegisterService,
        public top: TopRatedService,
        private route: Router,
        private data1: Data2Service,
    ) { }

    url = this.products.baseUrl;

    ngOnInit() {
        // this.products.authentication().toPromise().then(() => {
        //   console.log('fz');

        // })
        // .catch((error) => {
        //   console.log(error, 'fz');
        // });
        this.spinner.show();
        this.data.currentMessage.subscribe(msg => {
            console.log('from subscribe', msg);
            if (msg === '') {
                console.log('hide');
                this.show = 'hide';
                if (this.data.getLocalStorage('userData')) {
                    console.log('show');
                    this.show = 'show';
                }
            } else {
                console.log('header', msg);
                if (msg === 'show' || msg === 'hide') {

                    this.show = msg;
                }
            }
        });


        /**
         * Behaviour Subject => to get the count of the cart
         */
        this.data.currentMessage.subscribe(msg => this.count = msg);

        if (!this.data.getLocalStorage('cart')) {
            this.data.setLocalStorage('cart', []);
        }
        // this.c = JSON.parse(localStorage.getItem('cart'));
        // console.log('local--->', this.userData);
        this.counter();

        /**
         * API Call => To get all the top rated  products
         */
        this.products.getTopProducts().toPromise().then(data => {
            this.topProd = data.product_details;
            console.log('top--->', data.product_details);
            this.spinner.hide();

        })
            .catch((error) => {
                this.spinner.hide();

            });
        /**
         * API Call => To get all Carousel Images
         */
        this.products.getAllCategories().toPromise().then(data => {
            this.categoryImg = data.category_details;
            console.log('cat->>>', this.categoryImg);
            this.spinner.hide();

        })
            .catch((error) => {
                this.spinner.hide();

            });
    }
    allProd(categoryId, colorId) {
        this.spinner.show();
        this.products.getAllProducts(categoryId, colorId).toPromise().then(data => {
            console.log('allProd---->', data.product_details);
            this.prod = data.product_details;
            this.spinner.hide();
        })
            .catch((error) => {
                this.spinner.hide();

            });
    }

    /**
     * When clicked on the carousel image.
     * Send the data to product page to view the all the products similar to that product category
     * @param x => Object of the particular product
     */
    prodInfo(x: any) {
        console.log(x.category_id);
        this.route.navigate(['/productPage/' + x.category_id]);
    }

    /**
     * To view the details of the product.
     * @param itemLists => Object of the product
     */
    prodDetails(itemLists) {
        const key = 'subImages_id';
        const key2 = 'color_id';
        const value = itemLists.DashboardProducts[0].subImages[0];
        const value2 = itemLists.DashboardProducts[0].product_color[0];
        itemLists.DashboardProducts[0][key] = value;
        itemLists.DashboardProducts[0][key2] = value2;
        this.products.productDetails = itemLists.DashboardProducts[0];
        console.log(itemLists);
        this.route.navigate([
            '/productDetails/' + this.products.productDetails.product_id
        ]);
    }
    /**
     * Auto incrementing counter
     */
    counter() {
        $('.count').each(() => {
            console.log('counter', '1');
            $(this)
                .prop('Counter', 0)
                .animate(
                    {
                        Counter: $(this).text()
                    },
                    {
                        duration: 2000,
                        easing: 'swing',
                        step(now) {
                            $(this).text(Math.ceil(now));
                        }
                    }
                );
        });
    }

    /**
     * Add the product to cart
     * @param product => Object of the product
     * @param message => Snackbar message
     * @param action => Snackbar message
     */
    addToCart(product, message: string, action: string) {
        const c = this.data.getLocalStorage('cart');
        const key1 = 'quantity';
        const key2 = 'total';
        product[key1] = 1;
        product[key2] = product.product_cost;
        console.log('length', c.length);
        /**
         * For checking the duplicate product
         */
        let a = 0;
        c.forEach(element => {
            if (product._id === element._id) {
                a++;
                this.snackBar.open('Already Added to Cart', action, {
                    duration: 3000
                });
            }
        });
        if (a === 0) {
            c.push(product);
            this.data.setLocalStorage('cart', c);
            // c = JSON.parse(localStorage.getItem('cart'));
            this.data.cartCount(c.length);
            this.data1.count(c.length);
            this.snackBar.open(message, action, {
                duration: 3000
            });
        }
    }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterService } from '../register.service';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { forbiddenNameValidator } from '../validator.directive';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'app-recover-pass',
    templateUrl: './recover-pass.component.html',
    styleUrls: ['./recover-pass.component.css']
})
export class RecoverPassComponent implements OnInit {
    hide = true;
    hide1 = true;
    recoverPassForm: FormGroup;
    constructor(
        private fb: FormBuilder,
        private apiService: RegisterService,
        private route: Router,
        private data: DataService,
        private snackBar: MatSnackBar,
    ) {
        this.recoverPassForm = this.fb.group({
            otpCode: ['', Validators.required],
            newPass: ['', [Validators.required, forbiddenNameValidator(/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,12}$/)]],
            confirmPass: ['', [Validators.required, forbiddenNameValidator(/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,12}$/)]]
        }, {
                validator: this.passwordValidator
            });
    }

    ngOnInit() {
    }
    submit() {
        console.log(this.recoverPassForm.get('otpCode').value);
        console.log(this.data.getLocalStorage('tempData'));


        if (this.data.getLocalStorage('tempData').otp == this.recoverPassForm.get('otpCode').value) {
            console.log('1');

            this.apiService.recoverPass(this.recoverPassForm.value).toPromise().then((data) => {
                console.log(data);
                localStorage.removeItem('tempData');
                this.snackBar.open('Password Changed', '', {
                    duration: 3000,
                });

                this.route.navigate(['/login']);
            }).catch((error) => {

            });
        }
    }
    passwordValidator(form: FormGroup) {
        // console.log('error');

        const condition = form.get('newPass').value !== form.get('confirmPass').value;
        // console.log(condition, form.get('newPass').value, form.get('confirmPass').value);

        return condition ? { passwordsDoNotMatch: true } : null;
    }
    getErrorMessage() {
        return this.recoverPassForm.get('newPass').hasError('required') ? 'You must enter a value' :
            this.recoverPassForm.get('newPass').hasError('forbiddenName') ? 'Not a valid password' :
                '';
    }
    getErrorMessage1() {
        return this.recoverPassForm.get('confirmPass').hasError('required') ? 'You must enter a value' :
            this.recoverPassForm.get('confirmPass').hasError('forbiddenName') ? 'Not a valid password' :
                '';
    }

}

import { TestBed } from '@angular/core/testing';

import { TopRatedService } from './top-rated.service';

describe('TopRatedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TopRatedService = TestBed.get(TopRatedService);
    expect(service).toBeTruthy();
  });
});

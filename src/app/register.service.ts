import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Data2Service } from './data2.service';
import { DataService } from './data.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
@Injectable({
    providedIn: 'root'
})
export class RegisterService {
    baseUrl = 'http://10.0.100.210:3500/';
    // data = {};
    myCart = this.data.getLocalStorage('cart');
    show: string;
    productDetails: any;
    serachProd: any;
    CartData: any;
    constructor(
        private link: HttpClient,
        private data1: Data2Service,
        private data: DataService,
        private snackBar: MatSnackBar,
        private route: Router,
    ) {
        // this.userAuthentication();
    }
    register(msg): Observable<any> {
        console.log(msg);
        return this.link.post(this.baseUrl + 'register', msg);
    }
    login(): Observable<any> {
        // console.log(msg);
        return this.link.get(this.baseUrl + 'auth/google');
    }
    getLog(data): Observable<any> {
        console.log(data);
        return this.link.post(this.baseUrl + 'login', data);
    }
    getUser(msg): Observable<any> {
        console.log(msg);
        return this.link.put(this.baseUrl + 'profile', msg);
    }
    addAdress(msg): Observable<any> {
        console.log('servicemsg-->', msg);
        return this.link.post(this.baseUrl + 'address', msg);
    }
    getAddress(): Observable<any> {
        // console.log('servicemsg-->', msg);
        return this.link.get(this.baseUrl + 'getCustAddress');
    }
    getTopProducts(): Observable<any> {
        return this.link.get(this.baseUrl + 'defaultTopRatingProduct');
    }
    getAllCategories(): Observable<any> {
        return this.link.get(this.baseUrl + 'getAllCategories');
    }
    getAllProducts(x = '', y = '', a = '', b = '', c = '', pNo = 0, limit = 0): Observable<any> {
        console.log('a===>', a);
        return this.link.get(this.baseUrl +
            'commonProducts?category_id=' + x
            + '&color_id=' + y + '&sortBy=' + a +
            '&sortIn=' + b + '&name=' + c + '&pageNo=' + pNo + '&perPage=' + limit);
    }
    getAllColors(): Observable<any> {
        return this.link.get(this.baseUrl + 'getAllColors');
    }
    getAllProductsInAscending(): Observable<any> {
        return this.link.get(this.baseUrl + 'getAllProductsInAscending');
    }
    getAllProductsInDescending(): Observable<any> {
        return this.link.get(this.baseUrl + 'getAllProductsInDescending');
    }
    getTopRated() {
        return this.link.get(this.baseUrl + 'getAllProductsInHighestRating');
    }
    checkout(cart) {
        return this.link.post(this.baseUrl + 'addProductToCartCheckout', cart);
    }
    getOrderDetails() {
        return this.link.get(this.baseUrl + 'getOrderDetails');
    }
    getCartData() {
        return this.link.get(this.baseUrl + 'getCartData');
    }
    updateProductRating(rating) {
        return this.link.put(this.baseUrl + 'updateProductRatingByCustomer', rating);
    }
    deleteCartProduct(cart) {
        console.log('apibody', cart);
        return this.link.delete(this.baseUrl + 'deleteCustomerCart/' + cart.product_id);
    }
    setDeliveryAddress(address) {
        return this.link.put(this.baseUrl + 'updateAddress', address);
    }
    authentication() {
        return this.link.get(this.baseUrl + 'authenticateUser');
    }
    getProductDetails(x = ''): Observable<any> {
        // console.log('a===>', a);
        return this.link.get(this.baseUrl +
            'commonProducts?_id=' + x);
    }
    getInvoiceOfOrder(orderDetails) {
        console.log(orderDetails);
        return this.link.post(this.baseUrl + 'getInvoiceOfOrder', orderDetails);
    }
    delAddress(add) {
        return this.link.delete(this.baseUrl + 'deladdress/' + add.address_id);
    }
    editAddress(address) {
        return this.link.put(this.baseUrl + 'updateAddress', address);
    }
    getTermsAndConditions() {
        return this.link.get(this.baseUrl + 'getTermsAndConditions');
    }
    getPolicy() {
        return this.link.get(this.baseUrl + 'getGuarantee');
    }

    logout() {
        this.myCart = this.data.getLocalStorage('cart');
        const userData = this.data.getLocalStorage('userData');
        if (this.data.getLocalStorage('cart')) {

            userData.cartCount = this.myCart.length;
        } else {
            userData.cartCount = 0;
        }
        this.data.setLocalStorage('userData', userData);
        // localStorage.setItem('userData', JSON.stringify(userData));
        this.show = 'hide';
        this.data.changeMessage(this.show);
        console.log(this.myCart);
        if (this.myCart == null) {

        } else {

            this.myCart.push({ flag: 'logout' });
            this.data.setLocalStorage('cart', this.myCart);
            // localStorage.setItem('cart', JSON.stringify(this.myCart));
            this.checkout(this.myCart).subscribe((data) => {
                console.log(data);
            });
        }
        // setTimeout(() => {
        // }, 3000);
        localStorage.removeItem('cart');
        localStorage.removeItem('userData');
        localStorage.removeItem('customerDetails');
        this.data1.count(0);
        this.snackBar.open('Successfully Logout', '', {
            duration: 3000,
        });
        this.route.navigate(['/dashboard']);

    }

    logoutService() {
        this.myCart = this.data.getLocalStorage('cart');
        const userData = this.data.getLocalStorage('userData');
        if (this.data.getLocalStorage('cart')) { }
        userData.cartCount = this.myCart.length;
        this.data.setLocalStorage('userData', userData);
        // localStorage.setItem('userData', JSON.stringify(userData));
        this.show = 'hide';
        this.data.changeMessage(this.show);
        console.log(this.myCart);
        if (this.myCart == null) {

        } else {

            this.myCart.push({ flag: 'logout' });
            this.data.setLocalStorage('cart', this.myCart);
            // localStorage.setItem('cart', JSON.stringify(this.myCart));
            this.checkout(this.myCart).subscribe((data) => {
                console.log(data);
            });
        }
        setTimeout(() => {
            localStorage.removeItem('cart');
            localStorage.removeItem('userData');
            localStorage.removeItem('customerDetails');

        }, 3000);
        this.data1.count(0);
        // this.route.navigate(['/dashboard']);

    }


    proDetails() {

    }
    userAuthentication() {
        this.authentication().toPromise().then((data) => {
            console.log('auth', data);
        }).catch((error) => {
            console.log('not a user', error);

        });
    }
    forgotPass(email): Observable<any> {
        return this.link.post(this.baseUrl + 'forgotPassword', email);

    }
    recoverPass(data): Observable<any> {
        return this.link.post(this.baseUrl + 'recoverPassword', data);

    }
    changePass(data): Observable<any> {
        return this.link.post(this.baseUrl + 'changePassword', data);

    }
}

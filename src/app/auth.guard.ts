import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CanActivate } from '@angular/router';
import { DataService } from './data.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private route: Router, private data: DataService) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        if (state.url.includes('/login')) {
            if (this.data.getLocalStorage('userData')) {
                this.route.navigate(['/dashboard']);
                return false;
            } else {
                return true;
            }
        } else if (state.url.includes('/profile')) {
            if (this.data.getLocalStorage('userData')) {
                return true;
            } else {

                this.route.navigate(['/login']);

                return false;
            }
        } else if (state.url.includes('/editProfile')) {
            if (this.data.getLocalStorage('userData')) {
                return true;
            } else {

                this.route.navigate(['/login']);

                return false;
            }
        } else if (state.url.includes('/adress')) {
            if (this.data.getLocalStorage('userData')) {
                return true;
            } else {

                this.route.navigate(['/login']);

                return false;
            }
        } else if (state.url.includes('/order')) {
            if (this.data.getLocalStorage('userData')) {
                return true;
            } else {

                alert('Please login first');
                this.route.navigate(['/login']);

                return false;
            }
        } else if (state.url.includes('/addAddress')) {
            if (this.data.getLocalStorage('userData')) {
                return true;
            } else {

                this.route.navigate(['/login']);

                return false;
            }
        } else if (state.url.includes('/recover-pass')) {
            if (this.data.getLocalStorage('userData')) {
                this.route.navigate(['/home']);
                return false;
            } else {
                if (this.data.getLocalStorage('tempData')) {
                    return true;
                } else {

                    this.route.navigate(['/home']);
                    return false;
                }

            }
        } else if (state.url.includes('/change-pass')) {
            if (this.data.getLocalStorage('userData')) {
                return true;
            } else {
                this.route.navigate(['/error-page/']);
                return false;

            }
        } else if (state.url.includes('/forgot-pass')) {
            if (this.data.getLocalStorage('userData')) {
                this.route.navigate(['/error-page']);
                return false;
            } else {
                return true;

            }
        } else {
            // alert('not login')
            console.log(state.url);
        }
        // console.log(state.url.includes('/login'));
        // console.log(this.data.getLocalStorage('userData').token)
        // return false;
    }
}

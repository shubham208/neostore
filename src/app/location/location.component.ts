import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {
  title = 'My first AGM project';
  lat = 51.678418;
  lng = 7.809007;
  constructor() { }

  ngOnInit() {
    $('.count').each(function() {
      $(this).prop('Counter', 0).animate({
          Counter: $(this).text()
      }, {
          duration: 8000,
          easing: 'swing',
          step(now) {
              $(this).text(Math.ceil(now));
          }
      });
  });
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { forbiddenNameValidator } from '../validator.directive';
import * as moment from 'moment';
import { DataService } from '../data.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  profileData = this.data.getLocalStorage('customerDetails');
  constructor(private route: Router, private fb: FormBuilder, private data: DataService) { }
  // dob = moment(this.profileData.dob).format('DD-MM-YYYY');
  ngOnInit() {
    // console.log(this.dob);
    // console.log(this.profileData.customer_details.first_name);
  }
  edit() {
    this.route.navigate(['/editProfile']);

  }

}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as CryptoJS from 'crypto-js';
import * as CryptoTS from 'crypto-ts';
const SECRET_KEY = 'secret';
@Injectable({
    providedIn: 'root'
})
export class DataService {
    // property = 'show';
    // cart = JSON.parse(localStorage.getItem('cart'));
    private messageSource = new BehaviorSubject<string>('');
    private messageSource1 = new BehaviorSubject<string>('');
    private profilePic = new BehaviorSubject<string>('');
    pic = this.profilePic.asObservable();
    private rateProd = new BehaviorSubject<string>('');
    // private cartData =new BehaviorSubject();
    private count = new BehaviorSubject<any>('1');

    currentMessage = this.messageSource.asObservable();
    msgId = this.rateProd.asObservable();
    constructor() { }
    changeMessage(message: any) {
        this.messageSource.next(message);
    }
    display(property: any) {
        console.log('dataservice', property);
        this.messageSource1.next(property);

    }
    rate(proId: any) {
        // console.log('dataservice', property);
        this.rateProd.next(proId);

    }
    cartCount(count) {
        console.log('countDataService', count);

        this.count.next(count);

    }
    proPic(url) {
        this.profilePic.next(url);
    }
    get(key) {
        const k = localStorage.getItem(key);
        const decrypted = CryptoJS.AES.decrypt(k, 'Secret Passphrase');
        console.log(key, JSON.parse(decrypted.toString(CryptoJS.enc.Utf8)));
        return JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
    }

    set(key, value) {
        const encrypted = CryptoJS.AES.encrypt(JSON.stringify(value), 'Secret Passphrase');

        localStorage.setItem(key, encrypted.toString());
    }
    // getLocalStorage(key) {
    //     return JSON.parse(localStorage.getItem(key));

    // }
    // setLocalStorage(key, value) {
    //     localStorage.setItem(key, JSON.stringify(value));
    // }
    setLocalStorage(key, value) {
        const userEncryptedString = CryptoTS.AES.encrypt(JSON.stringify(value), SECRET_KEY);
        localStorage.setItem(key, userEncryptedString.toString());
    }
    getLocalStorage(key) {
        const tempUser = localStorage.getItem(key);
        let userDecryptedBytes;
        let userDecryptedText;
        if (tempUser) {
            userDecryptedBytes = CryptoTS.AES.decrypt(tempUser, SECRET_KEY);
            userDecryptedText = userDecryptedBytes.toString(CryptoTS.enc.Utf8);
        }
        return tempUser ? JSON.parse(userDecryptedText) : null;
    }
}


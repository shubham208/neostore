import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../register.service';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
    prod: any;
    url: any;
    date: any;
    address: any;
    invoice;
    newDate;
    displayError;
    constructor(private apiService: RegisterService, private spinner: NgxSpinnerService) { }
    ngOnInit() {
        /**
         * API Call => get all the orders placed by the user
         */
        this.spinner.show();
        this.url = this.apiService.baseUrl;
        this.apiService.getOrderDetails().toPromise().then((data: any) => {
            this.address = data.deliveryAddress;
            this.date = moment(data.product_details.createdAt).format('DD/MM/YYYY');
            this.prod = data.product_details;
            console.log('orderData->', data.product_details);
            if ((this.prod).length === 0) {
                console.log('noooo');
                this.displayError = true;

            }
            this.spinner.hide();

        }).catch(() => {
            this.spinner.hide();
        });
    }
    downloadInvoice(orderDetails) {
        // const key = 'deliveryAddress';
        // orderDetails[key] = this.address;
        console.log(orderDetails);
        this.newDate = moment(orderDetails.product_details[0].createdAt).format('Do MMM YYYY');
        orderDetails.createdAt = this.newDate;
        console.log(orderDetails);
        this.apiService.getInvoiceOfOrder(orderDetails).toPromise().then((data: any) => {
            this.invoice = this.apiService.baseUrl + data.receipt;
            console.log(this.invoice, 'invoice');
            window.open(this.invoice, '_blank');
        });
    }

}

import { Injectable } from '@angular/core';
import {
    CanDeactivate
} from '@angular/router';
// import { Comp1Component } from './comp1/comp1.component';
import { RegisterComponent } from './register/register.component';

@Injectable()
export class CanDeactivateGuard implements CanDeactivate<RegisterComponent> {
    canDeactivate(registComp: RegisterComponent): boolean {

        console.log('i am here');
        console.log(registComp.registerForm.valid);
        if (!registComp.registerForm.valid) {
            if (confirm('You have unsaved changes! If you leave, your changes will be lost.')) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}

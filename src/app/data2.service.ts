import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class Data2Service {
    // myCart = JSON.parse(localStorage.getItem('cart'));
    // userData = JSON.parse(localStorage.getItem('cart'));

    private subject = new BehaviorSubject<string>('');
    cartCount = this.subject.asObservable();
    private cartData = new BehaviorSubject<object>([]);
    data = this.cartData.asObservable();
    private cartDisplay = new BehaviorSubject<boolean>(true);
    show = this.cartDisplay.asObservable();

    constructor() { }
    count(count) {
        console.log('------------------------', count);
        // const c = JSON.parse(localStorage.getItem('cart'));
        this.subject.next(count);
    }
    getCartData(item) {
        this.cartData.next(item);
    }
    display(value) {
        this.cartDisplay.next(value);
    }

}

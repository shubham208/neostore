import { Component, OnInit } from '@angular/core';
import {
    FormGroup,
    FormControl,
    Validators,
    AbstractControl,
    FormBuilder,
    FormGroupDirective,
    NgForm
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { forbiddenNameValidator, CustomValidator, CustomValidatorForText } from '../validator.directive';

import { RegisterService } from '../register.service';
import { Router } from '@angular/router';
@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    hide = true;
    hide1 = true;
    constructor(private fb: FormBuilder, private registerUser: RegisterService, private route: Router) { }
    ngOnInit() {
        this.registerForm = this.fb.group({
            first_name: ['', [Validators.required, CustomValidatorForText.text]],
            last_name: ['', [Validators.required, CustomValidatorForText.text]],
            email: ['', [Validators.required, Validators.email]],
            pass: ['', [Validators.required, forbiddenNameValidator(/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,12}$/)]],
            confirmPass: ['', [Validators.required, forbiddenNameValidator(/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,12}$/)]],
            phone_no: ['', [
                Validators.required,
                Validators.minLength(10),
                Validators.maxLength(10),
                CustomValidator.numeric
            ]
            ],
            gender: ['Male']
        }, {
                validator: this.passwordValidator
            });
    }
    onSubmit(): void {
        this.registerUser.register(this.registerForm.value).subscribe();
        this.route.navigate(['/login']);
    }
    textValid() {
        return this.registerForm.get('first_name').hasError('required') ? 'You must enter a value' :
            this.registerForm.get('first_name').hasError('invalidString') ? 'Not valid' :
                '';
    }
    textValid1() {
        return this.registerForm.get('last_name').hasError('required') ? 'You must enter a value' :
            this.registerForm.get('last_name').hasError('invalidString') ? 'Not valid' :
                '';
    }
    getErrorMessageNumeric() {
        return this.registerForm.get('phone_no').hasError('required') ? 'You must enter a value' :
            this.registerForm.get('phone_no').hasError('invalidNumber') ? 'Only numbers allowed' :
                '';
    }
    passwordValidator(form: FormGroup) {
        console.log('error');

        const condition = form.get('pass').value !== form.get('confirmPass').value;
        // console.log(condition, form.get('pass').value, form.get('confirmPass').value);

        return condition ? { passwordsDoNotMatch: true } : null;
    }

    /**
     * Form Validation for the password field
     */
    getErrorMessage() {
        return this.registerForm.get('pass').hasError('required') ? 'You must enter a value' :
            this.registerForm.get('pass').hasError('forbiddenName') ? 'Not a valid password' :
                '';
    }
    getErrorMessage1() {
        return this.registerForm.get('confirmPass').hasError('required') ? 'You must enter a value' :
            this.registerForm.get('confirmPass').hasError('forbiddenName') ? 'Not a valid password' :
                '';
    }
}



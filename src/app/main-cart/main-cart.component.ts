import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { AddressComponent } from '../address/address.component';
import { DataService } from '../data.service';
import { Data2Service } from '../data2.service';
import { MatStepper, MatHorizontalStepper } from '@angular/material';

@Component({
    selector: 'app-main-cart',
    templateUrl: './main-cart.component.html',
    styleUrls: ['./main-cart.component.css']
})

export class MainCartComponent implements OnInit {
    cartProd: any;
    checked = true;
    disable;
    cart = this.data.getLocalStorage('cart');
    public add = [];
    @ViewChild(MatHorizontalStepper) stepper: MatHorizontalStepper;

    constructor(
        private route: Router,
        private service: RegisterService,
        private data: DataService,
        private data1: Data2Service
    ) { }
    userInfo: any;
    show = 'hide';
    showAddress;
    ngOnInit() {
        this.data1.show.subscribe((data) => {
            console.log('------->>>>>', data);
            this.disable = data;
        });

        // if (!this.data.getLocalStorage('cart')) {
        //     console.log('1');

        //     this.disable = true;
        // }
        // console.log(this.obj.bool);
        if (this.data.getLocalStorage('userData')) {
            if ((this.data.getLocalStorage('userData').customer_address).length !== 0) {
                if (this.data.getLocalStorage('userData').customer_address === 'You did not add your address.') {
                    this.data.getLocalStorage('userData').customer_address = [];
                    console.log('1');
                    this.showAddress = false;


                } else {
                    this.showAddress = true;

                    this.userInfo = this.data.getLocalStorage('userData');
                    console.log('==', this.userInfo);
                }

                // this.showAddress = true;
            } else {
                this.showAddress = false;
            }
            // this.userInfo = JSON.parse(localStorage.getItem('userData'));
            this.show = 'show';
        } else {
            this.show = 'hide';
        }
        // API Call => show the customer address
        // console.log((this.data.getLocalStorage('userData').customer_address).length);


    }
    addAddress() {
        this.route.navigate(['/addAddress']);
    }
    selectAddress() {
        // debugger;
        // console.log(this.data.getLocalStorage('cart').length);

        if (!this.data.getLocalStorage('userData')) { // check wheather the localstorage is present or not
            alert('Please login first');
            this.route.navigate(['/login']);
        } else if (this.data.getLocalStorage('cart')) {
            // confirm('yourCart is empty');
            // this.stepper.selectedIndex = 0;
            this.stepper.selected.completed = true;

            if (this.data.getLocalStorage('cart').length === 0) {

                this.stepper.selected.completed = true;
            }
            // if (confirm('Your Cart is empty, you must add some products to you shopping cart. Click on OK to add products ')) {

            //     this.route.navigate(['/productPage']);
            //     console.log('1');
            // } else {

            // }




        }

    }

    /**
     * set the isDeliveryAddress to true and then send the updated data to database
     */
    address(add) {
        console.log(add);
        add.isDeliveryAddress = true;
        this.service.setDeliveryAddress(add).subscribe((data) => {
            this.checked = false;
        });
    }
    editPage(id) {
        console.log(id);
        this.data.setLocalStorage('tempAdd', id);
        this.route.navigate(['/editAddress/' + id.address_id]);
    }
    /**
     * Send the order data to database and redirect to order placed component
     */
    checkout() {
        const cart = this.data.getLocalStorage('cart');
        cart.push({ flag: 'checkout' });
        this.data.setLocalStorage('cart', cart);
        // localStorage.setItem('cart', JSON.stringify(cart));
        this.service.checkout(cart).toPromise().then((data) => {
            console.log(data);

            localStorage.removeItem('cart');
            this.data1.count(0);

            this.route.navigate(['/order-placed']);
        }).catch((error) => {
            const cart1 = this.data.getLocalStorage('cart');

            console.log('error');
            console.log(cart1);


            cart1.splice(-1, 1);
            this.data.setLocalStorage('cart', cart1);

            console.log(cart1);

            // cart.pop({ flag: 'checkout' });
            // alert('Please select the Address');
        });
    }

}

import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DataService } from '../data.service';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
// import { star-ratin }
@Component({
  selector: 'app-subscriber',
  templateUrl: './subscriber.component.html',
  styleUrls: ['./subscriber.component.css']
})
export class SubscriberComponent implements OnInit {
  // @Input() msg: string;
msg: string;
  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.currentMessage.subscribe(msg => this.msg = msg);
  }
  onRate(event) {
    console.log(event, 'rating events-------------');
  }
}

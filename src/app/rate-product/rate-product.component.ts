import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../register.service';
import { DataService } from '../data.service';
import { ProductDetailsComponent } from '../product-details/product-details.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';
@Component({
  selector: 'app-rate-product',
  templateUrl: './rate-product.component.html',
  styleUrls: ['./rate-product.component.css']
})
export class RateProductComponent implements OnInit {
rate = {
  product_id : '',
  product_rating: '',
};
disable = true;
  constructor(private service: RegisterService, private data: DataService,
              public dialogRef: MatDialogRef<ProductDetailsComponent>,
              private route: Router,
              ) { }
  id: any;
  ngOnInit() {
    this.data.msgId.subscribe(msg => this.id = msg);
    console.log(this.id);
  }
  onRatingSet(event) {
    console.log(event);
    this.rate.product_id = this.id;
    this.rate.product_rating = event;
    this.service.updateProductRating(this.rate).toPromise().then(() => {
      this.disable = false;
    }).catch((error) => {
      this.disable = true;
      this.onNoClick();
      // alert('login');
      // this.route.navigate(['/login']);
      if (error.status === 403) {
        console.log('************************');
      }
    });

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}

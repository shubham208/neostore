import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { DataService } from '../data.service';

@Component({
    selector: 'app-profile-home',
    templateUrl: './profile-home.component.html',
    styleUrls: ['./profile-home.component.css']
})
export class ProfileHomeComponent implements OnInit {
    profileData = this.data.getLocalStorage('customerDetails');

    constructor(private route: Router, private imgService: RegisterService, private data: DataService) { }
    profilePic = this.imgService.baseUrl + this.profileData.profile_img;
    ngOnInit() {
        if (this.profileData.profile_img == null) {
            this.profilePic = '../../assets/images/profile-placeholder.png';
        }
        console.log(this.profilePic, 'dsfs');
    }
    profile() {
        this.route.navigate(['/profile']);
    }
    order() {
        this.route.navigate(['/order']);

    }
    address() {
        this.route.navigate(['/adress']);
    }

}

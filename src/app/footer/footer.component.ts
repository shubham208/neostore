import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';

// import {FormsModule} from '@angular/forms';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  subcriber = {
    email: ''
  };
msg: string;
pdf;

  constructor(private data: DataService, private route: Router, private service: RegisterService) { }

  ngOnInit() {
    /**
     * Behaviour Subject => To get the latest value
     */
    this.data.currentMessage.subscribe(msg => this.msg = msg);

  }
  newMsg() {
    /**
     * Behaviour Subject => Update the value of the input box to the data service
     */
    this.data.changeMessage(this.subcriber.email);
    this.route.navigate(['/subscriber']);

  }
  termsAndCond() {
    this.service.getTermsAndConditions().subscribe((data: any) => {
    this.pdf = data.termsAndConditions_details[0].fileName;
    window.open(this.service.baseUrl + this.pdf, '_blank');
  });
}
  policy() {
    this.service.getPolicy().subscribe((data: any) => {
      this.pdf = data.guarantee_details[0].fileName;
      window.open(this.service.baseUrl + this.pdf, '_blank');
    });
  }

}

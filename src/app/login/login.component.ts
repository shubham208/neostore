import { Component, OnInit } from '@angular/core';
import {
    FormControl,
    Validators,
    FormGroup,
    FormBuilder
} from '@angular/forms';
import { forbiddenNameValidator } from '../validator.directive';
import { RegisterService } from '../register.service';
import { Router } from '@angular/router';
import {
    AuthService,
    FacebookLoginProvider,
    GoogleLoginProvider
} from 'angular-6-social-login';
import { NgxSpinnerService } from 'ngx-spinner';

import { DataService } from '../data.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Data2Service } from '../data2.service';
import * as _ from 'lodash';
// import { ForgotPassComponent } from '../forgot-pass/forgot-pass.component';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    show = 'hide';
    constructor(
        private socialAuthService: AuthService,
        private fb: FormBuilder,
        private loginService: RegisterService,
        private route: Router,

        private data: DataService,
        private snackBar: MatSnackBar,
        private spinner: NgxSpinnerService,
        private data1: Data2Service,
    ) { }
    // email = new FormControl('', [Validators.required, Validators.email]);
    hide = true;
    cart: any;
    dataSource;
    ngOnInit() {
        /**
         * Form builder for loginForm
         */
        this.loginForm = this.fb.group({
            email: ['', [Validators.required, Validators.email]],
            pass: [
                '',
                [
                    Validators.required,
                    forbiddenNameValidator(
                        /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,12}$/
                    )
                ]
            ]
        });
    }
    /**
     * Social Login
     * @param socialPlatform => value of the social Platform
     */
    public socialSignIn(socialPlatform: string) {
        let socialPlatformProvider;
        if (socialPlatform === 'google') {
            socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
        }
        this.socialAuthService.signIn(socialPlatformProvider).then(userData => {
            console.log(socialPlatform + ' sign in data :', userData);
            // Now sign-in with userData
            // ...
        });
    }

    /**
     * Form Validation for password field
     */
    getErrorMessage() {
        return this.loginForm.get('pass').hasError('required')
            ? 'You must enter a value'
            : this.loginForm.get('pass').hasError('forbiddenName')
                ? 'Not a valid password'
                : '';
    }
    social() {
        this.loginService.login().subscribe((data) => {
            console.log(data);

        });
    }

    /**
     * Login
     * @param this.loginForm.value=> value of thhe loginForm
     */
    login() {
        this.loginService.getLog(this.loginForm.value).toPromise().then(data => {

            if (data.success === true) {
                this.spinner.show();
                console.log('profileData', data);


                this.data.proPic(data.customer_details.profile_img);

                console.log('login---->', data.customer_details);
                this.data.setLocalStorage('userData', data);
                this.data.setLocalStorage('customerDetails', data.customer_details);
                this.carta();
                // window.localStorage.setItem('userData', JSON.stringify(data)); // set the response to locall storage
                // window.localStorage.setItem('customerDetails', JSON.stringify(data.customer_details)
                this.show = 'show';
                this.data.changeMessage(this.show); // to enbale show hide function of login options
                this.data1.count(data.cart_count); // to update the cart count

                this.snackBar.open('Successfully Logged in', '', {
                    duration: 3000,
                });
                this.spinner.hide();
                this.route.navigate(['/dashboard']);

            } else {
                // alert(data.message); // Shows the error alert
            }
        }).catch((error) => {
            // alert(error.message);
        });
    }

    carta() {
        console.log('++++++++=====+>', this.cart);
        if (this.cart == null) {
            this.cart = [];
            if (this.data.getLocalStorage('userData')) {
                console.log('1');
                this.cartApi();
                this.spinner.hide();

            }
        } else {
            console.log('26');

            this.cart = this.data.getLocalStorage('cart');
            if (this.data.getLocalStorage('userData')) {
                this.cartApi();
                this.spinner.hide();

            }
        }
    }

    private cartApi() {
        this.cart = this.data.getLocalStorage('cart');
        this.loginService.getCartData().subscribe((data: any) => {
            if (data.message !== 'Your cart is empty. Please, first add products on your cart') {
                console.log('31', data);
                const a = _.unionBy(data.product_details.map((item) => {
                    const key = 'quantity';
                    const key1 = 'total';
                    item.product_id[key] = item.quantity;
                    item.product_id[key1] = item.product_id.product_cost;
                    return item.product_id;
                }), this.cart, '_id');
                this.cart = a;
                this.data.setLocalStorage('cart', this.cart);
                this.data1.count(this.cart.length);
            }
            // if (data.product_details !== undefined) {
            //   data.product_details.map((element, i) => {
            //     console.log(element);
            //     const key = 'quantity';
            //     const key1 = 'total';
            //     element.product_id[key] = element.quantity;
            //     element.product_id[key1] = element.product_id.product_cost;
            // // console.log(_.unionBy(this.data.getLocalStorage('cart'), data.product_details, '_id'));
            //     let a = 0;
            //     console.log(this.cart, 'asdgf');
            //     this.cart.forEach(item => {
            //       if (element.product_id._id === item._id) {
            //         a++;
            //       }
            //     });
            //     if (a === 0) {
            //       console.log('pushed');
            //       this.cart.push(element.product_id);
            //       this.data.setLocalStorage('cart', this.cart);
            //       this.data1.getCartData(this.cart);
            //       this.data1.count(this.cart.length);
            //       // window.location.reload();
            //       this.spinner.hide();
            //       this.loginService.CartData = this.cart;
            //       this.dataSource = this.cart;

            //     }
            //   });
            //   }
        });
    }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { forbiddenNameValidator } from '../validator.directive';
import { RegisterService } from '../register.service';
import { DataService } from '../data.service';
@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
    profileForm: FormGroup;
    minDate = new Date(1900, 0, 1);
    maxDate = new Date();
    constructor(
        private data: DataService,
        private route: Router,
        private fb: FormBuilder, private editProfileService: RegisterService) { }
    select = null;
    formData = new FormData();
    details = this.data.getLocalStorage('customerDetails');
    ngOnInit() {
        console.log(Date.now);
        /**
         * ProfileForm FormBuilder
         */
        this.profileForm = this.fb.group({
            firstName: [this.details.first_name, Validators.required],
            lastName: [this.details.last_name, Validators.required],
            gender: [this.details.gender, Validators.required],
            dob: [this.details.dob, Validators.required],
            mobile: [this.details.phone_no, Validators.required],
            emailID: [this.details.email, [Validators.required, Validators.email]],
        });
    }
    cancel() {
        this.route.navigate(['/profile']);
    }
    /**
     * Append Profile image to the Formdata
     * @param event => Chosen profile image
     */
    selectedImg(event) {
        this.select = event.target.files[0];
        this.formData.append('profile_img', this.select);
        console.log(this.formData);
    }
    /**
     * Append all the formControls to the formdata
     */
    save() {
        this.formData.append('first_name', this.profileForm.get('firstName').value);
        this.formData.append('last_name', this.profileForm.get('lastName').value);
        this.formData.append('gender', this.profileForm.get('gender').value);
        this.formData.append('dob', this.profileForm.get('dob').value);
        this.formData.append('phone_no', this.profileForm.get('mobile').value);
        this.formData.append('email', this.profileForm.get('emailID').value);

        console.log(this.formData);
        /**
         * API Call => Update Profile
         */
        this.editProfileService.getUser(this.formData).toPromise().then((item) => {
            console.log(item);
            this.data.setLocalStorage('customerDetails', item.customer_details);
            this.data.proPic(item.customer_details.profile_img);
            this.route.navigate(['/profile']);
        })
            .catch((error) => {
            });

    }

}

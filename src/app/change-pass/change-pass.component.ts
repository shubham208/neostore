import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterService } from '../register.service';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { forbiddenNameValidator } from '../validator.directive';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'app-change-pass',
    templateUrl: './change-pass.component.html',
    styleUrls: ['./change-pass.component.css']
})
export class ChangePassComponent implements OnInit {

    hide = true;
    hide1 = true;
    hide2 = true;

    changePassForm: FormGroup;
    constructor(
        private fb: FormBuilder,
        private apiService: RegisterService,
        private route: Router,
        private data: DataService,
        private snackBar: MatSnackBar,

    ) {
        this.changePassForm = this.fb.group({
            oldPass: ['', [Validators.required, forbiddenNameValidator(/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,12}$/)]],
            newPass: ['', [Validators.required, forbiddenNameValidator(/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,12}$/)]],
            confirmPass: ['', [Validators.required, forbiddenNameValidator(/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,12}$/)]]
        }, {
                validator: this.passwordValidator
            });
    }

    ngOnInit() {
    }
    submit() {
        // console.log(this.changePassForm.get('otpCode').value);
        this.apiService.changePass(this.changePassForm.value).toPromise().then((data) => {
            console.log(data);
            // localStorage.removeItem('tempData');
            this.snackBar.open('Password Changed', '', {
                duration: 3000,
            });

            this.route.navigate(['/login']);
        }).catch((error) => {

        });

    }
    passwordValidator(form: FormGroup) {
        // console.log('error');

        const condition = form.get('newPass').value !== form.get('confirmPass').value;
        // console.log(condition, form.get('newPass').value, form.get('confirmPass').value);

        return condition ? { passwordsDoNotMatch: true } : null;
    }
    getErrorMessage() {
        return this.changePassForm.get('newPass').hasError('required') ? 'You must enter a value' :
            this.changePassForm.get('newPass').hasError('forbiddenName') ? 'Not a valid password' :
                '';
    }
    getErrorMessage1() {
        return this.changePassForm.get('confirmPass').hasError('required') ? 'You must enter a value' :
            this.changePassForm.get('confirmPass').hasError('forbiddenName') ? 'Not a valid password' :
                '';
    }
    getErrorMessage2() {
        return this.changePassForm.get('oldPass').hasError('required') ? 'You must enter a value' :
            this.changePassForm.get('oldPass').hasError('forbiddenName') ? 'Not a valid password' :
                '';
    }

}

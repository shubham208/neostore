import { Directive } from '@angular/core';
import { ValidatorFn, FormGroup, ValidationErrors, AbstractControl } from '@angular/forms';
@Directive({
    selector: '[appValidator]'
})
export class ValidatorDirective {

    constructor() { }

}


/** A hero's name can't match the hero's alter ego */
/** A hero's name can't match the given regular expression */
export function forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        const forbidden = !nameRe.test(control.value);
        // console.log(forbidden);+
        return forbidden ? { forbiddenName: { value: control.value } } : null;
    };
}
export class CustomValidator {
    // Number only validation
    static numeric(control: AbstractControl) {
        const val = control.value;

        if (val === null || val === '') {

            return null;
        }
        if (!val.toString().match(/^[0-9]+(\.?[0-9]+)?$/)) {

            return { 'invalidNumber': true };
        }


        return null;
    }
}
export class CustomValidatorForText {
    // Number only validation
    static text(control: AbstractControl) {
        const val = control.value;

        if (val === null || val === '') {

            return null;
        }
        if (!val.toString().match(/^[A-Za-z]+$/)) {

            return { 'invalidString': true };
        }


        return null;
    }
}

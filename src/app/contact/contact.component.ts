import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidator } from '../validator.directive';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
    contactForm: FormGroup;
    constructor(private fb: FormBuilder) { }

    ngOnInit() {
        this.contactForm = this.fb.group({
            name: ['', Validators.required],
            email: ['',
                [Validators.required,
                Validators.email,
                ]
            ],
            mobno: ['', [Validators.required,
            CustomValidator.numeric,
            Validators.pattern('^[0-9]*$'),
            ]],
            subject: ['', [Validators.required]],
            message: ['', [Validators.required]]

        });
    }

}

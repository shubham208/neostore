import { Component, OnInit } from '@angular/core';
import { ProductInfoService } from '../product-info.service';
import { RegisterService } from '../register.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ShowOnDirtyErrorStateMatcher } from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';
import { trigger, animate, stagger, query, transition, style, useAnimation } from '@angular/animations';
import { bounce, rubberBand, flipInY, zoomIn, slideInLeft, jello } from 'ng-animate';
import { Data2Service } from '../data2.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../data.service';
import * as _ from 'lodash';
@Component({
    selector: 'app-product-page',
    templateUrl: './product-page.component.html',
    styleUrls: ['./product-page.component.css'],
    animations: [
        trigger('animation', [transition('* => *',
            query(':enter', [
                style({ opacity: 0 }),
                stagger(400, [
                    useAnimation(slideInLeft)
                ])
            ], { optional: true })
        )])
    ],
})
export class ProductPageComponent implements OnInit {
    prod: any;
    // this.c =

    // c: any = JSON.parse(localStorage.getItem('cart'));
    c;
    selCat: string;
    selColor: string;
    category;
    searchText: string;
    color: string;
    cat: any;
    panelOpenState = true;
    constructor(
        private snackBar: MatSnackBar,
        public prodList: RegisterService,
        private route: ActivatedRoute,
        private router: Router,
        private data1: Data2Service,
        private data: DataService,
        private spinner: NgxSpinnerService) { }
    url = this.prodList.baseUrl;
    categoryId = '';
    colorId = '';
    name;
    a = '2';
    // p = 4;
    limit = 8;
    pageNo = 1;
    totalCount;
    serachText: any;
    show = false;


    ngOnInit() {
        this.prodList.getAllCategories().subscribe((category) => {
            console.log('categorName------>', category.category_details);
            this.category = category.category_details;
            // this.prodList.userAuthentication();
            this.spinner.show();
            console.log('cartLocal----->', this.c);
            if (!this.data.getLocalStorage('cart')) {
                this.data.setLocalStorage('cart', []);
            }
            this.c = this.data.getLocalStorage('cart') || [];
            this.route.params.subscribe((data) => {
                // this.getCategories();
                console.log(data);
                if (data.id) {
                    console.log('if params');
                    this.category.map(category1 => {
                        if (category1._id === data.id) {
                            this.name = category1.category_name;
                            console.log(this.name, "name");
                        }
                    });
                    if (data.id) {
                        if (this.prodList.serachProd === undefined) {
                            console.log('searchexample123');
                            this.show = false;

                        } else {
                            this.spinner.hide();
                            this.totalCount = data.total_count;

                            this.prod = this.prodList.serachProd.product_details;
                            this.show = false;

                        }
                        this.categoryId = data.id;
                        this.prodList.getAllProducts(this.categoryId).subscribe((item) => {
                            console.log('allProd---->', item.product_details);
                            if (item.product_details === 'No details are available') {
                                console.log('searchexample');
                                this.prodList.getAllProducts('', '', '', '', data.id).toPromise().then((element) => {
                                    if (element.product_details !== 'No details are available') {
                                        this.totalCount = element.total_count;
                                        // console.log(element.product_details[0].category_id.category_name);
                                        this.name = element.product_details[0].category_id.category_name;
                                        this.prod = element.product_details;
                                        this.spinner.hide();
                                    } else {
                                        this.show = true;
                                        this.spinner.hide();
                                    }

                                })
                                    .catch(() => {
                                        this.spinner.hide();

                                    });

                            } else {
                                this.spinner.hide();
                                this.totalCount = data.total_count;

                                this.prod = item.product_details;
                            }
                        });
                    } else {
                        console.log(data.prodId);
                        this.serachText = JSON.parse(data.item1);
                        console.log('search----**>', this.serachText);
                        this.totalCount = data.total_count;
                        this.prod = this.serachText.product_details;
                        this.spinner.hide();

                    }
                } else {
                    // this.getCategories();
                    console.log('if  not params')

                    this.callPaginate(this.pageNo);
                }
            });

        });
        this.prodList.getAllColors().subscribe((data) => {
            console.log('colorName------>', data.color_details);
            this.color = data.color_details;
        });
    }
    /**
     * @param categoryId => holds the category id
     * @param colorId    => holds the category id
     * @param sortBy     => sort the products by cost or rating
     * @param sortIn     => true for increasing order and false for decreasing
     * @param search     => holds the search text value
     */
    selectedCategory(categoryId: any, colorId: any, sortBy: any, sortIn: any, search: any, name: any) {
        this.selCat = categoryId;
        this.name = name;
        this.selColor = colorId;
        this.pageNo = 1;
        this.searchText = search;
        this.prodList.getAllProducts(categoryId, colorId, sortBy, sortIn, search, this.pageNo, this.limit).subscribe((data) => {
            console.log('allProd---->', data.product_details);
            this.totalCount = data.total_count;
            if (data.success === true) {
                console.log(this.show);
                this.show = false;
                this.prod = data.product_details;
                if (this.selCat !== undefined) {
                    this.router.navigate(['/productPage/' + (this.selCat)]);
                }
            } else {
                this.show = true;
                console.log(this.show);
            }
        });

    }
    /**
     * @param product   =>object of the product that we want to add in cart
     */
    addToCart(product, message: string, action: string) {
        const key1 = 'quantity';
        const key2 = 'total';
        product[key1] = 1;
        product[key2] = product.product_cost;
        console.log('length', this.c.length);
        console.log(this.c);
        console.log(_.find(this.c, { _id: product._id }));
        if (!_.find(this.c, { _id: product._id })) {
            this.c.push(product);

            this.snackBar.open('Added', action, {
                duration: 3000,
            });
        } else {
            this.snackBar.open('Already Added to the Cart', action, {
                duration: 3000,
            });
        }
        this.data.setLocalStorage('cart', this.c);
        // localStorage.setItem('cart', JSON.stringify(this.c));
        this.data1.count(this.c.length);
        // let a = 0;
        // this.c.forEach(element => {
        //   if (product._id === element._id) {
        //     a++;
        //     this.snackBar.open('Already Added to the Cart', action, {
        //       duration: 3000,
        //     });
        //   }
        // });
        // if (a === 0) {
        //   this.c.push(product);
        //   this.data.setLocalStorage('cart', this.c);
        //   // localStorage.setItem('cart', JSON.stringify(this.c));
        //   this.data1.count(this.c.length);

        //   this.snackBar.open(message, action, {
        //     duration: 3000,
        //   });
        // }
    }
    /**
     * Product Details
     * @param itemLists =>object of the product
     */
    prodDetails(itemLists) {
        console.log('prodetails', itemLists);
        this.prodList.productDetails = itemLists;
        console.log('prodetails2', this.prodList.productDetails);

        this.router.navigate(['/productDetails/' + itemLists._id]); // send the id to productDetails component
    }
    topRated() {
        this.prodList.getTopRated().subscribe((data: any) => {
            this.prod = data.product_details;
            console.log('sortTopRated--->', data.product_details);
        });
    }
    getPageNumber(data) {
        console.log(data);
        const paginationObj = {
            pageNo: data,
            perPage: 7
        };
        this.callPaginate(data);
    }

    callPaginate(d) {
        this.pageNo = d;
        this.prodList.getAllProducts(this.selCat, this.selColor, '', '', '', d, this.limit).toPromise().then((item) => {
            this.totalCount = item.total_count;
            this.name = 'All Categories';
            console.log(item, 'sdfgsadf');
            this.prod = item.product_details;
            console.log(this.prod.length);



            console.log(this.prod, '------------------');
            this.spinner.hide();
        })
            .catch(() => {
                this.spinner.hide();

            });
    }
}

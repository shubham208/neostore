import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as CryptoJS from 'crypto-js';
import sha256 from 'crypto-js/sha256';
import hmacSHA512 from 'crypto-js/hmac-sha512';
import Base64 from 'crypto-js/enc-base64';
import { DataService } from '../data.service';


const subject = new BehaviorSubject(123);
@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {
  // data = ;
  plainText = 'shubham Soni';
  userData = this.data.getLocalStorage('userData');
  encryptText: string;
  encPassword: string;
  decPassword: string;
  conversionEncryptOutput: string;
  conversionDecryptOutput: string;
obj = {
  category_id: "5cfe3c5aea821930af69281e",
createdAt: "2019-06-10T11:26:57.346Z",
product_cost: 59999,
product_dimension: "84 * 28",
product_id: "5cfe3e81b4db0f338946eabc",
product_image: "2019-06-24T11-25-49.768ZFNSF51WCCO3_-_main_2.jpg",
product_material: "Cobalt, Material",
product_name: "Winchester Fabric Sofa",
product_producer: "Winchester Pvt. Ltd.",
product_rating: 3.75,
product_stock: 229,
quantity: 1,
subImages_id: "5cfe3a737b35112d9faa2f68",
total: 59999
};
  constructor(private data: DataService) { }

  ngOnInit() {
    // const encrypted = CryptoJS.AES.encrypt(JSON.stringify(this.userData), 'Secret Passphrase');
    // console.log(encrypted.toString());
    // const decrypted = CryptoJS.AES.decrypt(encrypted, 'Secret Passphrase');
    // console.log('decrypt', JSON.parse(decrypted.toString(CryptoJS.enc.Utf8)));
    // this.data.setLocalStorage('demo', JSON.stringify(encrypted.toString()));
    // this.data.
    // localStorage.setItem('demo', encrypted.toString());
    // console.log(JSON.parse(localStorage.getItem('demo')));

    // this.data.set('demo1', this.obj);
    // console.log(this.data.get('demo1'));
  }
}

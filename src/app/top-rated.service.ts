import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TopRatedService {
  topRatedProd = {
    'success': true,
    'message': 'Top Rating Product',
    'products': [
      {
        '_id': 104,
        'DashboardProducts': [
          {
            '_id': 312,
            'category_id': 104,
            'color_id': 211,
            'product_name': 'Twain Study Table',
            'product_image': 'http://180.149.241.208:3000/storage/image/FNSF51WCCO3_-_main_2.jpg',
            'prod_desc': 'Full marks. A classic desk with pared down trestle legs, the Twain study table is elegant in its simplicity. The generous table has room for you to spread out, and the two deep drawers help keep a check on overflowing papers, files, and more. There’s plenty of space below for legs to stretch comfortably.',
            'product_rating': 3.4,
            'product_producer': 'Alibaba',
            'product_cost': 6000,
            'product_stock': 300,
            'created_at': '2019-04-18T11:06:31.913Z',
            'product_dimension': '28 * 40',
            'product_material': 'Engineered Wood',
            '__v': 0
          }
        ]
      },
      {
        '_id': 102,
        'DashboardProducts': [
          {
            '_id': 305,
            'category_id': 102,
            'color_id': 210,
            'product_name': 'Robinson',
            'product_image': 'http://180.149.241.208:3000/storage/image/820170629075720img.jpg',
            'product_desc': 'Smart style. The Robinson bed set is made for comfortable sleep. The curved headboard is made with a designer dual tone finish. The central console is curved with a premium laminate finish. The bed comes with standard with Hydraulic & Manual storage for a complete storage solution. The box cut design and finish ensure that this bed is made for the modern Indian home. Pair this bed with its matching nightstand, chest of drawers and a choice of wardrobes for the complete bedroom set.',
            'product_rating': 3.6,
            'product_producer': 'somu stores',
            'product_cost': 40000,
            'product_stock': 1000,
            'created_at': '2019-04-18T10:39:51.830Z',
            'product_dimension': '206 * 30',
            'product_material': 'Matte',
            '__v': 0
          }
        ]
      },
      {
        '_id': 103,
        'DashboardProducts': [
          {
            '_id': 309,
            'category_id': 103,
            'color_id': 227,
            'product_name': 'Danum Swing Chair',
            'product_image': 'http://180.149.241.208:3000/storage/image/Ursula_Lounge_chair_BL_00.jpg',
            'product_desc': 'A comfortable enclosing swing chair\\nThe brown artificial rattan and tan cushion fabric are both weather-proof',
            'product_rating': 4,
            'product_producer': 'Urban ladder',
            'product_cost': 14000,
            'product_stock': 200,
            'created_at': '2019-04-18T10:56:23.021Z',
            'product_dimension': '39 * 72',
            'product_material': 'Fabric, Metal',
            '__v': 0
          }
        ]
      },
      {
        '_id': 103,
        'DashboardProducts': [
          {
            '_id': 309,
            'category_id': 103,
            'color_id': 227,
            'product_name': 'Danum Swing Chair',
            'product_image': 'http://180.149.241.208:3000/storage/image/Ursula_Lounge_chair_BL_00.jpg',
            'product_desc': 'A comfortable enclosing swing chair\\nThe brown artificial rattan and tan cushion fabric are both weather-proof',
            'product_rating': 4,
            'product_producer': 'Urban ladder',
            'product_cost': 14000,
            'product_stock': 200,
            'created_at': '2019-04-18T10:56:23.021Z',
            'product_dimension': '39 * 72',
            'product_material': 'Fabric, Metal',
            '__v': 0
          }
        ]
      },
      {
        '_id': 103,
        'DashboardProducts': [
          {
            '_id': 309,
            'category_id': 103,
            'color_id': 227,
            'product_name': 'Danum Swing Chair',
            'product_image': 'http://180.149.241.208:3000/storage/image/Ursula_Lounge_chair_BL_00.jpg',
            'product_desc': 'A comfortable enclosing swing chair\\nThe brown artificial rattan and tan cushion fabric are both weather-proof',
            'product_rating': 4,
            'product_producer': 'Urban ladder',
            'product_cost': 14000,
            'product_stock': 200,
            'created_at': '2019-04-18T10:56:23.021Z',
            'product_dimension': '39 * 72',
            'product_material': 'Fabric, Metal',
            '__v': 0
          }
        ]
      },
      {
        '_id': 103,
        'DashboardProducts': [
          {
            '_id': 309,
            'category_id': 103,
            'color_id': 227,
            'product_name': 'Danum Swing Chair',
            'product_image': 'http://180.149.241.208:3000/storage/image/Ursula_Lounge_chair_BL_00.jpg',
            'product_desc': 'A comfortable enclosing swing chair\\nThe brown artificial rattan and tan cushion fabric are both weather-proof',
            'product_rating': 4,
            'product_producer': 'Urban ladder',
            'product_cost': 14000,
            'product_stock': 200,
            'created_at': '2019-04-18T10:56:23.021Z',
            'product_dimension': '39 * 72',
            'product_material': 'Fabric, Metal',
            '__v': 0
          }
        ]
      },
      {
        '_id': 103,
        'DashboardProducts': [
          {
            '_id': 309,
            'category_id': 103,
            'color_id': 227,
            'product_name': 'Danum Swing Chair',
            'product_image': 'http://180.149.241.208:3000/storage/image/Ursula_Lounge_chair_BL_00.jpg',
            'product_desc': 'A comfortable enclosing swing chair\\nThe brown artificial rattan and tan cushion fabric are both weather-proof',
            'product_rating': 4,
            'product_producer': 'Urban ladder',
            'product_cost': 14000,
            'product_stock': 200,
            'created_at': '2019-04-18T10:56:23.021Z',
            'product_dimension': '39 * 72',
            'product_material': 'Fabric, Metal',
            '__v': 0
          }
        ]
      },
      {
        '_id': 101,
        'DashboardProducts': [
          {
            '_id': 303,
            'category_id': 101,
            'color_id': 216,
            'product_name': 'Apollo Sectional Sofa',
            'product_image': 'http://180.149.241.208:3000/storage/image/Twain_Study_table_00.jpg',
            'product_desc': 'A lot of care has gone into choosing the materials that go into making your sofa. And with continued care, they will share your address for many years.',
            'product_rating': 4,
            'product_producer': 'Apollo Pvt. Ltd.',
            'product_cost': 50000,
            'product_stock': 200,
            'created_at': '2019-04-18T10:28:02.019Z',
            'product_dimension': '84 * 28',
            'product_material': 'Wood, Metal',
            '__v': 0
          }
        ]
      }
    ]
  }
  constructor() { }
}

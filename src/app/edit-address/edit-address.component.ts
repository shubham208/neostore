import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { RegisterService } from '../register.service';
import * as _ from 'lodash';
import { DataService } from '../data.service';
import { CustomValidator, CustomValidatorForText } from '../validator.directive';

@Component({
    selector: 'app-edit-address',
    templateUrl: './edit-address.component.html',
    styleUrls: ['./edit-address.component.css']
})
export class EditAddressComponent implements OnInit {
    id;
    addressForm: FormGroup;
    constructor(
        private fb: FormBuilder,
        private route: Router,
        private addressService: RegisterService,
        private router: ActivatedRoute,
        private data: DataService) { }

    ngOnInit() {
        const dataAdd = this.data.getLocalStorage('tempAdd');
        console.log(dataAdd);

        this.addressForm = this.fb.group({
            // address_id: ['']
            address: [dataAdd.address, Validators.required],
            pincode: [dataAdd.pincode,
            [Validators.required,
            CustomValidator.numeric,
            Validators.pattern('^[0-9]*$'),
            Validators.maxLength(6),
            Validators.minLength(6)]
            ],

            city: [dataAdd.city, [Validators.required, CustomValidatorForText.text]],
            state: [dataAdd.state, [Validators.required, CustomValidatorForText.text]],
            country: [dataAdd.country, [Validators.required, CustomValidatorForText.text]]

        });
        this.router.params.subscribe((data) => {
            this.id = data.id;
            console.log(this.id);
        });
    }

    /**
     * Auto increment the size of text box
     * @param i =>index
     */
    textAreaAdjust(i) {
        const id = 'text' + i;
        const a = document.getElementById(id);
        a.style.height = '1px';
        a.style.height = 25 + a.scrollHeight + 'px';
    }

    /**
     * API Call => add new address
     */
    editAddress() {
        /**
         * @param this.addressForm.value => value of the ful form in JSON format
         */
        this.router.params.subscribe((data) => {
            this.id = data.id;
            console.log(this.id);
        });
        const key = 'address_id';
        this.addressForm.value[key] = this.id;
        this.addressService.editAddress(this.addressForm.value).toPromise().then((data) => {
            console.log('editaddress', data);
            const userData = this.data.getLocalStorage('userData');
            userData.customer_address.map((item, i) => {
                console.log(item.address_id);
                if (item.address_id == this.id) {
                    userData.customer_address[i] = this.addressForm.value;
                    this.data.setLocalStorage('userData', userData);
                    console.log('1');
                }
            });
            // userData.customer_address.push(data.address);
            // if (!_.find(userData.customer_address,  {_id: this.id})) {
            //   userData.customer_address[i] =
            // }
            // localStorage.setItem('userData', JSON.stringify(userData));
            this.route.navigate(['/adress']);
        })
            /**
             * it will catch error if oocurs
             */
            .catch((error) => {

            });
    }
    cancelBtn() {
        this.route.navigate(['/adress']);
    }
    getErrorMessage() {
        return this.addressForm.get('pincode').hasError('required') ? 'You must enter a value' :
            this.addressForm.get('pincode').hasError('invalidNumber') ? 'Not a valid pincode' :
                '';
    }
    countryFieldValid() {
        return this.addressForm.get('country').hasError('required') ? 'You must enter a value' :
            this.addressForm.get('country').hasError('invalidString') ? 'Not a valid country name' :
                '';

    }
    cityFieldValid() {
        return this.addressForm.get('city').hasError('required') ? 'You must enter a value' :
            this.addressForm.get('city').hasError('invalidString') ? 'Not a valid city name' :
                '';

    }
    stateFieldValid() {
        return this.addressForm.get('state').hasError('required') ? 'You must enter a value' :
            this.addressForm.get('state').hasError('invalidString') ? 'Not a valid state name' :
                '';

    }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { DataService } from '../data.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-forgot-pass',
    templateUrl: './forgot-pass.component.html',
    styleUrls: ['./forgot-pass.component.css']
})
export class ForgotPassComponent implements OnInit {
    recoverPassForm: FormGroup;
    constructor(
        public route: Router,
        private apiService: RegisterService,
        private data: DataService,
        public fb: FormBuilder
    ) { }

    ngOnInit() {
        this.recoverPassForm = this.fb.group({
            email: ['']
        });


    }
    submit() {
        console.log(this.recoverPassForm.value);

        this.apiService.forgotPass(this.recoverPassForm.value).toPromise().then((data) => {
            console.log(data);
            this.data.setLocalStorage('tempData', data);
            this.route.navigate(['/recover-pass']);
        }).catch((error) => {

        });
    }
}

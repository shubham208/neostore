import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
// import { NgxSpinnerService } from 'ngx-spinner';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { errorHandler } from '@angular/platform-browser/src/browser';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { RegisterService } from './register.service';
@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
    // constructor(private spinner: NgxSpinnerService) {}
    // public getFeed: any;
    getFeed = this.data.getLocalStorage('userData');
    constructor(private route: Router, private data: DataService, private apiCall: RegisterService) {
        // this.apiCall.userAuthentication();
    }
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        this.getFeed = this.data.getLocalStorage('userData');
        if (this.getFeed == null || this.getFeed === undefined) {
            console.log('system ko aag lga');
            if (this.data.getLocalStorage('tempData')) {
                console.log('1');
                request = request.clone({
                    setHeaders: {
                        Authorization: `bearer ${this.data.getLocalStorage('tempData').token}`
                    }
                });
            }
            // console.log('interceptor--->', JSON.parse(localStorage.getItem('userData')));
            // debugger;
        } else {
            // console.log('interceptor--->', JSON.parse(localStorage.getItem('userData')));
            console.log('system ko aag lga', this.getFeed);
            request = request.clone({
                setHeaders: {
                    Authorization: `bearer ${this.getFeed.token}`
                }
            });
        }
        return next.handle(request).pipe(
            // retry(1),
            catchError((error: HttpErrorResponse) => {
                let errorMessage = '';
                if (error.error instanceof ErrorEvent) {
                    // client-side error
                    errorMessage = `Error: ${error.error.message}`;
                } else {
                    // server-side error
                    errorMessage = `Error Code: ${error.status}\nMessage: ${
                        error.message
                        }`;
                    // alert(error.error.message);
                    if (error.status === 403) {
                        console.log('inter');
                        // this.route.navigate(['/login']);
                    } else if (error.status === 404) {
                        alert(error.error.message);
                    } else {
                        this.route.navigate(['/error-page/' + error.status]);

                    }
                }
                // this.spinner.hide();
                // this.spinner.hide();
                return throwError(errorMessage);
            })
        );

    }
}

import {
    SocialLoginModule,
    AuthServiceConfig,
    GoogleLoginProvider,
    FacebookLoginProvider,
} from 'angular-6-social-login';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyOwnCustomMaterialModule } from 'src/material';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
// import { SliderPipe } from './slider.pipe';
// import {StarRatingModule} from '/home/webwerks/neostore1/node_modules/star-ratings';
// import {MatButtonModule} from '@angular/material/button';
// import {StarRatingModule} from 'star-ratings';
import { ProductPageComponent } from './product-page/product-page.component';
import { LoginComponent } from './login/login.component';
import { CartComponent } from './cart/cart.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { AddressComponent } from './address/address.component';
import { AddAddressComponent } from './add-address/add-address.component';
import { EditAddressComponent } from './edit-address/edit-address.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { OrdersComponent } from './orders/orders.component';
import { DeliveryAddressComponent } from './delivery-address/delivery-address.component';
import { MainCartComponent } from './main-cart/main-cart.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ValidatorDirective } from './validator.directive';
import { LocationComponent } from './location/location.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { HttpErrorInterceptor } from './http-error.interceptor';
import { NgxInputStarRatingModule } from 'ngx-input-star-rating';
// import { StarRatingModule } from 'angular-star-rating';
import { SubscriberComponent } from './subscriber/subscriber.component';
import { ProfileHomeComponent } from './profile-home/profile-home.component';
import { RatingModule } from 'ng-starrating';
import { RegisterService } from './register.service';
import { ErrorPageComponent } from './error-page/error-page.component';
import { NgxStarsModule } from 'ngx-stars';
import { AuthGuard } from './auth.guard';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { NgxPaginationModule } from 'ngx-pagination';
// import { OrderComponent } from './order/order.component';
import { OrderPlacedComponent } from './order-placed/order-placed.component';
import { RateProductComponent } from './rate-product/rate-product.component';
import { CanDeactivateGuard } from './canDeactivateGuard';
// import { NgxSpinnerService } from 'ngx-spinner';
import { NgxLoadingModule } from 'ngx-loading';
import { MomentModule } from 'ngx-moment';
import { DemoComponent } from './demo/demo.component';
import { DocumentComponent } from './document/document.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { StockPipe } from './stock.pipe';
// import { ShareButtonsModule } from 'ngx-sharebuttons';
import { ShareModule } from '@ngx-share/core';
import { ContactComponent } from './contact/contact.component';
import { ForgotPassComponent } from './forgot-pass/forgot-pass.component';
import { RecoverPassComponent } from './recover-pass/recover-pass.component';
import { ChangePassComponent } from './change-pass/change-pass.component';
// import { ForgotPassComponent } from './forgot-pass/forgot-pass.component';

export function getAuthServiceConfigs() {
    const config = new AuthServiceConfig(
        [
            {
                id: GoogleLoginProvider.PROVIDER_ID,
                provider: new GoogleLoginProvider('784408118590-pks1oenr5fif2vrhvohv4dmrhas99c5m.apps.googleusercontent.com')
            }
        ]
    );
    return config;
}

@NgModule({
    entryComponents: [RateProductComponent],
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        DashboardComponent,
        // SliderPipe,
        ProductPageComponent,
        LoginComponent,
        CartComponent,
        RegisterComponent,
        ProfileComponent,
        ProductDetailsComponent,
        AddressComponent,
        AddAddressComponent,
        // NgxSpinnerService,
        EditAddressComponent,
        EditProfileComponent,
        OrdersComponent,
        DeliveryAddressComponent,
        MainCartComponent,
        ValidatorDirective,
        LocationComponent,
        SubscriberComponent,
        ProfileHomeComponent,
        ErrorPageComponent,
        // OrderComponent,
        // NgxSpinnerModule,
        OrderPlacedComponent,
        RateProductComponent,
        DemoComponent,
        DocumentComponent,
        StockPipe,
        ContactComponent,
        ForgotPassComponent,
        RecoverPassComponent,
        ChangePassComponent,
        // ForgotPassComponent,

        // ReactiveFormsModule
        // MyOwnCustomMaterialModule
    ],
    imports: [
        NgxPaginationModule,
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MyOwnCustomMaterialModule,
        ReactiveFormsModule,
        HttpClientModule,
        RatingModule,
        MomentModule,
        NgxSpinnerModule,
        SocialLoginModule,
        PdfViewerModule,
        NgxInputStarRatingModule,
        NgxStarsModule,
        // JwSocialButtonsModule,
        ShareModule,
        // FontAwesomeModule,
        // ShareButtonsModule.forRoot(),
        NgxLoadingModule.forRoot({}),
        NgxImageZoomModule.forRoot(),
        AgmCoreModule.forRoot(
            {
                apiKey: 'AIzaSyAhQHbmSDnt5LyyMbbwcYKpv0ZIp1KbJr4'
            }),
    ],
    providers: [RegisterService, RateProductComponent,

        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpErrorInterceptor,
            multi: true
        },
        {
            provide: AuthServiceConfig,
            useFactory: getAuthServiceConfigs
        }, AuthGuard, CanDeactivateGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

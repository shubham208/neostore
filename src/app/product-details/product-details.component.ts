import { Component, OnInit } from '@angular/core';
import { ActionSequence } from 'protractor';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { OrderPlacedComponent } from '../order-placed/order-placed.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { RateProductComponent } from '../rate-product/rate-product.component';
import { DataService } from '../data.service';
import { Data2Service } from '../data2.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ShareService } from '@ngx-share/core';

import * as _ from 'lodash';


@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.component.html',
    styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
    prodDetails: any;
    c;

    constructor(
        public share: ShareService,
        private data: DataService,
        public dialog: MatDialog,
        private router: Router,
        private route: ActivatedRoute,
        private details: RegisterService,
        private snackBar: MatSnackBar,
        private data1: Data2Service,
        private spinner: NgxSpinnerService) { }
    url = this.details.baseUrl;
    urlSubImg: any;
    img: any;
    animal: string;
    name: string;
    id: any;
    mainImg: any;
    ngOnInit() {
        this.spinner.show();
        if (!this.data.getLocalStorage('cart')) {
            // localStorage.setItem('cart', JSON.stringify([]));
            this.data.setLocalStorage('cart', []);

        }
        // this.c = JSON.parse(localStorage.getItem('cart'));
        if (this.details.productDetails !== undefined) {
            this.prodDetails = this.details.productDetails;
            this.spinner.hide();

            console.log('df', this.prodDetails);

        } else {
            this.route.params.subscribe((data) => {
                this.details.getProductDetails(data.id).toPromise().then((item) => {
                    this.prodDetails = item.product_details[0];
                    console.log(item, 'hello');
                    this.mainImg = this.url + this.prodDetails.product_image;
                    this.spinner.hide();

                    this.mainImg = this.url + this.prodDetails.product_image;
                });
            });
        }

        // this.route.params.subscribe((data) => {
        //   this.prodDetails =  JSON.parse(data.item);
        //   console.log('proDetails--------->', JSON.parse(data.item));
        //   // console.log('proDetails--------->', this.prodDetails.subImages_id);

        // });
        this.mainImg = this.url + this.prodDetails.product_image;
        // document.getElementById('id0').style.border = '2px solid black';
        // console.log(this.mainImg);
    }
    /**
     * Trigger when rate product is clicked
     * opens the dialog in which rate-component is rendered
     */
    openDialog(): void {
        this.data.rate(this.prodDetails._id);
        if (this.data.getLocalStorage('userData')) {

            const dialogRef = this.dialog.open(RateProductComponent, {
                width: '300px',
                height: '250px',
                data: { proid: this.prodDetails._id }
            });
        } else {
            alert('Login First');
            this.router.navigate(['/login']);
        }

        // dialogRef.afterClosed().subscribe(result => {
        //   console.log('The dialog was closed');
        //   this.animal = result;
        // });
    }

    /**
     * Swaps the sub images by the main
     * @param sub => sub images url
     */
    imageChange(sub, i) {
        this.urlSubImg = this.url + sub;
        this.mainImg = this.urlSubImg;
        const id = 'id' + i;
        // document.getElementById(id).style.border = '2px solid #E8272C';
        // console.log(this.urlSubImg);

    }
    /**
     * Add to cart
     * @param product => Object of the product
     */
    addToCart(product) {
        this.c = this.data.getLocalStorage('cart');
        const key1 = 'quantity';
        const key2 = 'total';
        product[key1] = 1;
        product[key2] = product.product_cost;
        console.log('length', this.c.length);
        console.log(_.find(this.c, { _id: product._id }));
        if (!_.find(this.c, { _id: product._id })) {
            this.c.push(product);

            this.snackBar.open('Added', '', {
                duration: 3000,
            });
        } else {
            this.snackBar.open('Already Added to the Cart', '', {
                duration: 3000,
            });
        }
        this.data.setLocalStorage('cart', this.c);
        // localStorage.setItem('cart', JSON.stringify(this.c));
        this.data1.count(this.c.length);
        // let a = 0;
        // this.c.forEach(element => {
        //   if (product._id === element._id) {
        //     a++;
        //     this.snackBar.open('Already Added to the Cart', '', {
        //       duration: 3000,
        //     });
        //   }
        // });
        // if (a === 0) {          // Checking the duplication
        //   this.c.push(product);
        //   // localStorage.setItem('cart', JSON.stringify(this.c));
        //   this.data.setLocalStorage('cart', this.c);
        //   this.data1.count(this.c.length);

        //   this.snackBar.open('Added to the Cart', '', {
        //     duration: 3000,
        //   });
        // }
    }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'stock'
})
export class StockPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        // console.log(value);
        if (value > 0) {
            return 'In Stock';
        } else {
            return 'Out of Stock';
        }

        // return null;
    }

}

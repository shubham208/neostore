import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../register.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';
// @Pipe({ name: 'safe' })
// import { DomSanitizer} from '@angular/platform-browser';
@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {
url;
mainUrl;
docName;
pdf;
aa;
  constructor(public sanitizer: DomSanitizer, private service: RegisterService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.url = this.service.baseUrl;
    this.route.params.subscribe((data) => {
      this.docName = data.id;
      console.log(this.docName);
    });
    if (this.docName === 'termsAndCondition') {
      this.service.getTermsAndConditions().subscribe((data: any) => {
        this.pdf = data.termsAndConditions_details[0].fileName;
        this.mainUrl = this.url + this.pdf;
        this.aa = this.sanitizer.bypassSecurityTrustResourceUrl(this.mainUrl);
        console.log(this.mainUrl);
      });
    } else {
      this.service.getTermsAndConditions().subscribe((data: any) => {
        this.pdf = data.guarantee_details[0].fileName;
        this.mainUrl = this.url + this.pdf;
        this.aa = this.sanitizer.bypassSecurityTrustResourceUrl(this.mainUrl);
      });

    }
    // this.url = this.service.baseUrl;
  }


}

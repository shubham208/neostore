import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { group } from '@angular/animations';
import { MatSnackBar } from '@angular/material';
import { Data2Service } from '../data2.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { userInfo } from 'os';
import { DataService } from '../data.service';
import { timingSafeEqual } from 'crypto';
import { ArrayDataSource } from '@angular/cdk/collections';
import * as _ from 'lodash';


@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})

export class CartComponent implements OnInit {
    form: FormGroup = this.fb.group({
        quantites: this.fb.array([])
    });
    cart: any = [];
    userInfo;
    displayedColumns: string[] = ['product', 'quantity', 'price', 'total', 'delete'];
    dataSource = this.data.getLocalStorage('cart');
    quantityValue;
    key = 'quantity';
    value: any;
    total: number;
    subTotal: number;
    disable = true;
    totalProPrice = 0;
    showContent;
    constructor(
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private urlService: RegisterService,
        private snackBar: MatSnackBar,
        private data1: Data2Service,
        private data: DataService,
        private spinner: NgxSpinnerService,

        private changeDetectorRefs: ChangeDetectorRef) {
    }
    url = this.urlService.baseUrl;
    ngOnInit() {
        if (!this.data.getLocalStorage('cart')) {
            console.log('1');

            this.data.setLocalStorage('cart', []);
            this.disable = true;
        }
        this.cart = this.data.getLocalStorage('cart') || [];
        console.log('2');
        // setTimeout(() => this.carta(),
        // 5000);
        this.data1.data.subscribe((data) => {
            console.log(data);
            // this.cart = data;
        });
        // localStorage.setItem('cart', JSON.stringify(this.cart));
        this.toggleProccedBtn();
        this.form = this.fb.group({
            quantites: this.fb.array([])
        });
        // this.carta();
        if (this.cart != null) {
            this.cart.forEach((item) => {
                this.quantityArray.push(this.addGroup());
            });
            this.cart.forEach((element, i) => {
                this.setValue(this.quantityArray, i, element.quantity);
            });
        }
        this.spinner.hide();

    }

    /**
     * API Call => To get the user cart data when user login
     */
    carta() {
        console.log('++++++++=====+>', this.cart);
        if (this.cart == null || this.cart === []) {
            if (this.data.getLocalStorage('userData')) {
                console.log('1');
                this.cartApi();
                this.spinner.hide();

            }
        } else {
            console.log('26');

            this.cart = this.data.getLocalStorage('cart');
            if (this.data.getLocalStorage('userData')) {
                this.cartApi();
                this.spinner.hide();

            }
        }

    }

    private cartApi() {
        this.urlService.getCartData().subscribe((data: any) => {
            console.log('31', data);
            console.log(this.cart);
            if (data.product_details !== undefined) {
                data.product_details.map((element, i) => {
                    console.log(element);
                    const key = 'quantity';
                    const key1 = 'total';
                    element.product_id[key] = element.quantity;
                    element.product_id[key1] = element.product_id.product_cost;
                    let a = 0;
                    if (this.cart != null) {
                        this.cart.forEach(item => {
                            if (element.product_id._id === item._id) {
                                a++;
                            }
                        });

                        if (a === 0) {
                            console.log('pushed');
                            this.cart.push(element.product_id);
                            this.data.setLocalStorage('cart', this.cart);
                            this.data1.getCartData(this.cart);
                            this.data1.count(this.cart.length);
                            // window.location.reload();
                            this.spinner.hide();
                            this.urlService.CartData = this.cart;
                            this.dataSource = this.cart;

                        }
                    }

                });
                // setTimeout(() => {
                //   this.dataSource = this.cart;
                //   console.log(this.dataSource, '/////////////////');
                // }, 2000);
                // window.location.reload();
                // }
                // console.log('cart data-->', data.product_details[0] );
                // this.changeDetectorRefs.detectChanges();
            }
            // console.log(this.urlService.CartData, 'sdfgsdgb-----', this.cart);
        });



    }



    toggleProccedBtn() {
        // console.log('length', JSON.parse(localStorage.getItem('cart')).length);
        if (this.data.getLocalStorage('cart')) {
            if (this.data.getLocalStorage('cart').length > 0) {
                this.disable = false;
                this.data1.display(false);

            } else {
                this.disable = true;
                this.data1.display(true);


            }
        } else {
            this.disable = false;
            this.data1.display(false);
        }
    }
    totalPrice(i) {
        // this.cart = JSON.parse(localStorage.getItem('cart'));
        this.totalProPrice = this.cart[i].quantity * this.cart[i].product_cost;
        return this.totalProPrice;

    }

    /**
     * get quantity form as form array it will make new input box at new index of the form
     * array
     */

    get quantityArray() {
        return this.form.get('quantites') as FormArray;
    }

    /**
     * @returns new form group 'quantity'
     */

    addGroup(): FormGroup {
        return this.fb.group({
            quantity: [1]
        });
    }

    /**
     * get the value of the input box (Form Group)
     * @param arr => gives form groups which is stored in Form Array
     * @param pos => index
     * @returns value of the input box
     */

    getValue(arr: FormArray, pos): number {
        // console.log(arr.value[pos].quantity);
        this.quantityValue = arr.value[pos].quantity;
        // console.log(this.quantityValue, '***************');
        return (arr.value[pos].quantity);

    }

    /**
     * set the value of the input box (Form Group)
     * @param arr =>gives Form Groups which is stored in Form Array
     * @param pos => index
     * @param value => value to be set in the input box
     */

    setValue(arr: FormArray, pos, value: number) {
        // console.log(value, '-----');
        arr.at(pos).patchValue({
            quantity: value,
        });
    }

    /**
     * increase quantity
     * @param i => index of the form array
     */

    inc(i) {
        // this.cart = this.data.getLocalStorage('cart');
        const val = this.getValue(this.quantityArray, i);
        if (val >= 9) {
            alert('Limit Reached');
        } else {
            this.setValue(this.quantityArray, i, this.getValue(this.quantityArray, i) + 1);
            this.cart[i].quantity = this.getValue(this.quantityArray, i);
            this.cart[i].total = this.cart[i].quantity * this.cart[i].product_cost;
            this.data.setLocalStorage('cart', this.cart);
            console.log(this.cart[i]);
        }
    }

    /**
     * decrease quantity
     * @param i => index of the from array
     */
    dec(i) {
        // this.cart = JSON.parse(localStorage.getItem('cart'));

        const val = this.getValue(this.quantityArray, i);
        if (val <= 1) {
            console.log('0');
        } else {
            this.setValue(this.quantityArray, i, this.getValue(this.quantityArray, i) - 1);
            this.cart[i].quantity = this.getValue(this.quantityArray, i);
            this.cart[i].total = this.cart[i].quantity * this.cart[i].product_cost;
            console.log(this.cart[i].total);
            this.data.setLocalStorage('cart', this.cart);
        }

    }

    // ************ Total Price Calculation ******************

    getTotalCost() {
        // console.log('total');
        // this.cart = JSON.parse(localStorage.getItem('cart'));

        if (!this.data.getLocalStorage('cart')) {
            return 0;
        } else {
            this.subTotal = this.cart.map(t => t.total).reduce((acc, value) => acc + value, 0);
            return this.subTotal;
        }
    }

    // ************ Total Cart Price ******************


    getOrderTotal() {
        // this.cart = JSON.parse(localStorage.getItem('cart'));

        if (!this.data.getLocalStorage('cart')) {
            return 0;
        } else {
            return Math.round(this.subTotal * (5 / 100)) + this.subTotal;
        }
    }

    // ************ Tax Calculation ******************
    getTax() {
        // this.cart = JSON.parse(localStorage.getItem('cart'));

        if (!this.data.getLocalStorage('cart')) {
            return 0;
        } else {
            return Math.round(this.subTotal * (5 / 100));
        }
    }

    /**
     * delete product from the cart
     * @param cart => object of the selected product
     */
    deleteProduct(cart) {
        // console.log('dele', cart.product_id);
        if (confirm('Are you sure you want to delete this item from cart')) {
            if (this.data.getLocalStorage('userData')) {

                this.urlService.deleteCartProduct(cart).toPromise().then((data) => {
                    console.log('delete', data);
                })
                    .catch((error) => {
                    });
            }
            this.cart.forEach((element, i) => {
                if (cart._id === this.cart[i]._id) {
                    console.log('del');
                    this.cart.splice(i, 1);
                    this.data.setLocalStorage('cart', this.cart);
                    this.data1.count(this.cart.length);
                }
            });
            this.toggleProccedBtn();
            this.dataSource = this.dataSource.filter((value, key) => {
                return value._id !== cart._id;
            });
            this.snackBar.open('Product Deleted', '', {
                duration: 3000,
            });
        }
    }
}



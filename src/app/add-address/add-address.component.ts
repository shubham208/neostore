import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { DataService } from '../data.service';
import { CustomValidator, CustomValidatorForText } from '../validator.directive';

@Component({
    selector: 'app-add-address',
    templateUrl: './add-address.component.html',
    styleUrls: ['./add-address.component.css']
})
export class AddAddressComponent implements OnInit {
    addressForm: FormGroup;
    constructor(
        private fb: FormBuilder,
        private route: Router,
        private addressService: RegisterService,
        private data: DataService) { }

    ngOnInit() {
        this.addressForm = this.fb.group({
            address: ['', Validators.required],
            pincode: ['',
                [Validators.required,
                CustomValidator.numeric,
                Validators.pattern('^[0-9]*$'),
                Validators.maxLength(6),
                Validators.minLength(6)]
            ],
            city: ['', [Validators.required, CustomValidatorForText.text]],
            state: ['', [Validators.required, CustomValidatorForText.text]],
            country: ['', [Validators.required, CustomValidatorForText.text]]

        });
    }
    getErrorMessage() {
        return this.addressForm.get('pincode').hasError('required') ? 'You must enter a value' :
            this.addressForm.get('pincode').hasError('invalidNumber') ? 'Not a valid pincode' :
                '';
    }
    countryFieldValid() {
        return this.addressForm.get('country').hasError('required') ? 'You must enter a value' :
            this.addressForm.get('country').hasError('invalidString') ? 'Not a valid country name' :
                '';

    }
    cityFieldValid() {
        return this.addressForm.get('city').hasError('required') ? 'You must enter a value' :
            this.addressForm.get('city').hasError('invalidString') ? 'Not a valid city name' :
                '';

    }
    stateFieldValid() {
        return this.addressForm.get('state').hasError('required') ? 'You must enter a value' :
            this.addressForm.get('state').hasError('invalidString') ? 'Not a valid state name' :
                '';

    }

    /**
     * Auto increment the size of text box
     * @param i =>index
     */
    textAreaAdjust(i) {
        const id = 'text' + i;
        const a = document.getElementById(id);
        a.style.height = '1px';
        a.style.height = 25 + a.scrollHeight + 'px';
    }

    /**
     * API Call => add new address
     */
    addAddress() {
        /**
         * @param this.addressForm.value => value of the ful form in JSON format
         */
        this.addressService.addAdress(this.addressForm.value).toPromise().then((data) => {
            console.log(data);
            const userData = this.data.getLocalStorage('userData');
            // debugger;
            console.log(typeof (userData.customer_address));

            if (userData.customer_address === 'You did not add your address.') {
                userData.customer_address = [];
            }
            userData.customer_address.push(data.address);
            this.data.setLocalStorage('userData', userData);
            //   localStorage.setItem('userData', JSON.stringify(userData));
            this.route.navigate(['/adress']);
            console.log(userData);

        })
            /**
             * it will catch error if oocurs
             */
            .catch((error) => {

            });
    }
    cancelBtn() {
        this.route.navigate(['/adress']);
    }

}
